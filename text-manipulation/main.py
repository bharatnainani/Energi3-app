def main (search_text,replace_text):
    search_text = search_text
    replace_text = replace_text

    # Opening our text file in read only
    with open(r'data.txt', 'r') as file:
        # Reading the content of the file
        # them in a new variable
        data = file.read()
        # Searching and replacing the text
        data = data.replace(search_text, replace_text)

    # Opening our text file in write only
    with open(r'data.txt', 'w') as file:
        file.write(data)

if __name__ == "__main__":
    search_text = "Jack"
    replace_text = "Jake"
    main(search_text, replace_text)
