# Build Dockerfile

## Overview

Question 1
The following is the Dockerfile for energi3

## Deployment

For running it locally

```
docker build -t energi3 .
docker run --name energi3 -p 39796:39796 -p 39797:39797 energi3:latest
```

For building it automatically we have pipeline for the same
https://gitlab.com/bharatnainani/Energi3-app/-/blob/main/.gitlab-ci.yml

