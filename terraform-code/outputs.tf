output "role_arn" {
  value = aws_iam_role.energi_role.arn
}

output "policy_arn" {
  value = aws_iam_policy.energi_policy.arn
}

output "user_arn" {
  value = aws_iam_user.energi_user.arn
}

output "group_arn" {
  value = aws_iam_group.energi_iam_group.arn
}