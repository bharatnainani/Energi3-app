# IAM 

## Overview

Question 6
This code is responsible for creating IAM roles, IAM policy, IAM group and IAMusers and attaching users to group

<!-- BEGIN_TF_DOCS -->
## Requirements

| Name | Version |
|------|---------|
| <a name="requirement_terraform"></a> [terraform](#requirement\_terraform) | ~> 1.0 |
| <a name="requirement_aws"></a> [aws](#requirement\_aws) | ~> 3.0 |

## Providers

| Name | Version |
|------|---------|
| <a name="provider_aws"></a> [aws](#provider\_aws) | ~> 3.0 |

## Modules

No modules.

## Resources

| Name | Type |
|------|------|
| [aws_iam_role.energi_role](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/iam_role) | resource |
| [aws_iam_policy.energi_policy](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/iam_policy) | resource |
| [aws_iam_group.energi_iam_group](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/iam_group) | resource |
| [aws_iam_group_policy_attachment.attach_policy](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/iam_group_policy_attachment) | resource |
| [aws_iam_user.energi_user](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/iam_user) | resource |
| [aws_iam_user_group_membership.adding_user](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/iam_user_group_membership) | resource |
| [aws_caller_identity.current](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/data-sources/caller_identity) | data source |



## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:--------:|
| <a name="input_region"></a> [region](#input\_region) | AWS region | `string` | n/a | yes |
| <a name="input_project"></a> [project](#input\_project) | Project name | `string` | phable | yes |
| <a name="input_environment"></a> [environment](#input\_environment) | Environment name | `string` | app | yes |
| <a name="input_role_name"></a> [role_name](#input\_role_name) | AWS role name | `string` | prod-ci-role | yes |
| <a name="input_policy_name"></a> [policy_name](#input\_policy_name) | AWS policy name | `string` | prod-ci-policy | yes |
| <a name="input_user_name"></a> [user_name](#input\_user_name) | AWS user name | `string` | prod-ci-user|  yes |
| <a name="input_group_name"></a> [group_name](#input\_group_name) | AWS group name | `string` | prod-ci-group | yes |


## Outputs

| Name | Description |
|------|-------------|
| <a name="output_role_arn"></a> [alb\_role\_name](#output\_role\_name) | AWS IAM role ARN |
| <a name="output_policy_arn"></a> [alb\_policy\_name](#output\_policy\_name) | AWS policy name |
| <a name="output_user_arn"></a> [alb\_user\_name](#output\_user\_name) | AWS user name |
| <a name="output_group_arn"></a> [alb\_group\_name](#output\_group\_name) | AWS group name |

<!-- END_TF_DOCS -->

## Development

### Prerequisites

- [terraform](https://learn.hashicorp.com/terraform/getting-started/install#installing-terraform)
- [terraform-docs](https://github.com/segmentio/terraform-docs)
