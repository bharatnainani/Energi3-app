variable "region" {
  description = "AWS region"
  type        = string
  default = "us-east-1"
}

variable "environment" {
  description = "Application environment name (dev/prod/qa)"
  type        = string
  default     = "dev"
}

variable "project" {
  description = "Project or organisation name"
  type        = string
  default     = "energi"
}

variable "role_name" {
  description = "IAM role"
  type        = string
  default     = "prod-ci-role"
}

variable "policy_name" {
  description = "IAM policy"
  type        = string
  default     = "prod-ci-policy"
}

variable "user_name" {
  description = "IAM user"
  type        = string
  default     = "prod-ci-user"
}

variable "group_name" {
  description = "IAM Group"
  type        = string
  default     = "prod-ci-group"
}
